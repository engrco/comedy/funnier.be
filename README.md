## Not the Bee -- the Babylon MurderHornet

MEAN, mutant stinging satire of the kind that you didn't think would chase you around.

If you're a [Bee](https://babylonbee.com/) who came here looking for flattery, how much imitation did you want? If we can see further, it's because this MurderHornet stands on the [Babylon Bee](https://babylonbee.com/)'s shoulders
